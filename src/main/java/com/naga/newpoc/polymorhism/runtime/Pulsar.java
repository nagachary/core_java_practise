package com.naga.newpoc.polymorhism.runtime;

public class Pulsar extends TwoWheeler {

	@Override
	public String bikeCC() {
		return "180CC";
	}

	public String bikeMaker() {
		return "Bajaj";
	}

}
