package com.naga.newpoc.polymorhism.runtime;

public class MainClass {

	public static void main(String[] args) {
		HondaCity hc = new HondaCity();

		System.out.println("Car Maker hc :" + hc.carMaker());
		System.out.println("Car Model hc :" + hc.carModel());
		hc.carType();
		System.out.println("No Of Tyres :" + hc.noOfTyres());

		ToyotaCamry tc = new ToyotaCamry();

		System.out.println("Car Maker tc :" + tc.carMaker());
		System.out.println("Car Model tc :" + tc.carModel());
		tc.carType();
		System.out.println("No Of Tyres :" + tc.noOfTyres());
		
		FourWheeler fw1 = hc;
		fw1.carType();
		
		FourWheeler fw2 = tc;
		fw2.carType();
		
		FourWheeler fw3 = (FourWheeler)tc;
		fw3.carType();
		

		Apache ap = new Apache();

		System.out.println("Apache Bike CC :" + ap.bikeCC());
		System.out.println("Apache Bike Maker :" + ap.bikeMaker());
		System.out.println("No Of Tyres :" + ap.noOfTyres());

		Pulsar pl = new Pulsar();

		System.out.println("Pulsar Bike CC :" + pl.bikeCC());
		System.out.println("Pulsar Bike Maker :" + pl.bikeMaker());
		System.out.println("No Of Tyres :" + pl.noOfTyres());
		
		TwoWheeler tw = new TwoWheeler();
		System.out.println("Two Wheeler Bike CC :" + tw.bikeCC());
		
		TwoWheeler tw1 = ap;
		System.out.println("Two Wheeler Apache Bike CC :" + tw1.bikeCC());
		TwoWheeler tw2 = pl;
		System.out.println("Two Wheeler Pulsar Bike CC :" + tw2.bikeCC());

	}

}
