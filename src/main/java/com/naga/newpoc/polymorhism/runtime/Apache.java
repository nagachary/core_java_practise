package com.naga.newpoc.polymorhism.runtime;

public class Apache extends TwoWheeler {
	
	@Override
	public String bikeCC() {
		return "160CC";
	}
	
	public  String bikeMaker(){
		return "Apache";
	}

}
