package com.naga.newpoc.polymorhism.runtime;

public class ToyotaCamry  extends FourWheeler {
	

	@Override
	public String carMaker() {
		return "Toyota";
	}
	
	@Override
	public void carType() {
		System.out.println("ToyotaCamry Car is Luxurious");
	}
	
	public String carModel() {
		return "Coupe";
	}

}
