package com.naga.newpoc.polymorhism.runtime;

public abstract class FourWheeler implements Vehicle {

	private final int NO_OF_TYRES = 4;
	
	@Override
	public int noOfTyres() {
		return NO_OF_TYRES;
	}
	
	public abstract String carMaker();
	
	public void carType(){
		System.out.println("Basic Car");
	}
	
	
	
}
