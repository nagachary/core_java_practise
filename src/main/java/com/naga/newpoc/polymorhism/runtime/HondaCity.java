package com.naga.newpoc.polymorhism.runtime;

public class HondaCity extends FourWheeler {

	public String carModel() {
		return "Sedan";
	}

	@Override
	public String carMaker() {
		return "Honda";
	}

}
