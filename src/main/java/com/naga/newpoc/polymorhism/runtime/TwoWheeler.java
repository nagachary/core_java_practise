package com.naga.newpoc.polymorhism.runtime;

public class TwoWheeler implements Vehicle {

	private final int NO_OF_TYRES = 2;

	@Override
	public int noOfTyres() {
		return NO_OF_TYRES;
	}

	public String bikeCC() {
		return "100CC";
	}
}
