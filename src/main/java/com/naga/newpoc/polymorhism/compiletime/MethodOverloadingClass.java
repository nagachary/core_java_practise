package com.naga.newpoc.polymorhism.compiletime;

public class MethodOverloadingClass {

	public int add(int x, int y) {
		//int c = x + y;
		//System.out.println("addition of x :" + x + " y :" + y + " is :=" + c);
		//return c;
		return x+y;
	}

	public int add(int x, int y, int z) {
	//	int c = x + y + z;
	//	System.out.println("addition of x :" + x + " y :" + y + " z :" + z
	//			+ " is :=" + c);
	//	return c;
		return x+y+z;
	}

	public int add(int x, double y) {
		//int c = x + (int) y;
		//System.out.println("addition of x :" + x + " y :" + y + " is :=" + c);
		//return c;
		
		return x+(int)y;
	}

	public int add(double x, int y) {
	//	int c = (int) x + y;
	//	System.out.println("addition of x :" + x + " y :" + y + " is :=" + c);
	//	return c;
		
		return (int) x + y;
	}
	
	public int add(double x, double y) {
	//	int c = (int)(x + y);
	//	System.out.println("addition of x :" + x + " y :" + y + " is :=" + c);
	//	return c;
		return (int)(x + y);
	}
	
}
