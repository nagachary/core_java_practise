package com.naga.newpoc.polymorhism.compiletime;

public class MainClass {
	
	public static void main(String []args){
		
		MethodOverloadingClass moc = new MethodOverloadingClass();
		
		System.out.println(moc.add(1, 2));
		System.out.println(moc.add(1, 1.1));
		System.out.println(moc.add(1.1,2));
		System.out.println(moc.add(1.1, 2.2));
		System.out.println(moc.add(1, 2, 3));
	}

}
