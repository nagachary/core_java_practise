package com.naga.newpoc.lamda.expressions;

public class FirstLamdaImplClass {

	public void implLamdaInter(FirstLamdaInterface flInterface) {
		flInterface.showStatus("fgh", "efg");
		flInterface.printValues("fgh", "efg", "def");
	}

	public static void main(String[] args) {
		FirstLamdaImplClass implClass = new FirstLamdaImplClass();
		
		/*FirstLamdaInterface interfaceRef = new FirstLamdaInterface() {
			public void showStatus(String v1, String v2) {
				if (v1.equals(v2))
					System.out.println("both val1 and val2 are equal");
				else
					System.out.println("both val1 and val2 are not equal");
			}
		};*/
		implClass.implLamdaInter( new FirstLamdaInterface() {
			public void showStatus(String v1, String v2) {
				if (v1.equals(v2))
					System.out.println("both val1 and val2 are equal");
				else
					System.out.println("both val1 and val2 are not equal");
			}
			
			public void printValues(String v1, String v2, String v3){}
		});
		
		implClass.implLamdaInter( new FirstLamdaInterface() {
			public void printValues(String v1, String v2, String v3) {
				if (v1 != null && v2 !=null && v3 != null)
					System.out.println("v1 =:"+v1+": v2 =:"+v2+": v3 =:"+v3);
				else
					System.out.println("one or some of the values are null ");
			}
			
			public void printValues1(String v1, String v2, String v3){
				
			}

			public void showStatus(String v1, String v2) {
				// TODO Auto-generated method stub
				
			}
		});
		

	}
}
