package com.naga.newpoc.lamda.expressions;

import org.springframework.stereotype.Component;

@Component
public interface Vehicle {
	
	public Vehicle getVehicleModel();
	public String getVehicleYear();
	public String getVehicleCost();

}
