package com.naga.newpoc.lamda.expressions;

public interface FirstLamdaInterface {

	public void showStatus(String v1, String v2);
	public void printValues(String v1, String v2, String v3);

}
